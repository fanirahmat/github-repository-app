package com.fanirahmat.myapplication.ui.detail

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fanirahmat.myapplication.data.model.Owner
import com.fanirahmat.myapplication.data.model.RepositoryResponse
import com.fanirahmat.myapplication.functions
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailViewModel : ViewModel() {
    private val dataRepo = MutableLiveData<RepositoryResponse>()
    private val dataOwnerRepo = MutableLiveData<Owner>()


    fun setRepository(repo:String) {
        functions.getMainApiService()?.getDetailRepository(
            repo
        )?.enqueue(object :
            Callback<RepositoryResponse> {
            override fun onResponse(
                call: Call<RepositoryResponse>,
                response: Response<RepositoryResponse>
            ) {
                if (response.isSuccessful) {
                    val body = response.body()
                    getDetailOwner(body?.owner?.url!!)
                    dataRepo.postValue(body)
                }
            }

            override fun onFailure(call: Call<RepositoryResponse>, t: Throwable) {
                Log.e("getRepositories", t.toString())
            }

        })
    }

    fun getDetailOwner(url:String) {
        Log.i("url", url.substringAfter("https://api.github.com/"))
        functions.getMainApiService()?.getDetailOwnerRepository()?.enqueue(object : Callback<Owner> {
            override fun onResponse(call: Call<Owner>, response: Response<Owner>) {
                if (response.isSuccessful) {
                    val body = response.body()
                    dataOwnerRepo.postValue(body)
                }
            }

            override fun onFailure(call: Call<Owner>, t: Throwable) {
                Log.e("getDetailOwner", t.toString())
            }

        })
    }

    fun getRepository() : LiveData<RepositoryResponse> {
        return dataRepo
    }

    fun getOwner() : LiveData<Owner> {
        return dataOwnerRepo
    }
}