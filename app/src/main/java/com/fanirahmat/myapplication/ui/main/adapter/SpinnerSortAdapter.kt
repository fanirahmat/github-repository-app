package com.fanirahmat.myapplication.ui.main.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.fanirahmat.myapplication.R

class SpinnerSortAdapter(
    context: Context,
    list : List<String>
) : ArrayAdapter<String>(context, 0, list) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return initView(position, convertView, parent)
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return initView(position, convertView, parent)
    }

    private fun initView(position: Int, convertView: View?, parent: ViewGroup) : View {
        val result = getItem(position)
        val view = convertView ?: LayoutInflater.from(context).inflate(R.layout.item_sort_spinner, parent, false)
        val value = view.findViewById<TextView>(R.id.namaPC)
        value.text = result
        return view
    }
}