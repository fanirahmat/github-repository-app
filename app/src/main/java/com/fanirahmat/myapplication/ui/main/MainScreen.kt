package com.fanirahmat.myapplication.ui.main

import android.content.Intent
import android.opengl.Visibility
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Adapter
import android.widget.LinearLayout
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.fanirahmat.myapplication.R
import com.fanirahmat.myapplication.databinding.ActivityMainBinding
import com.fanirahmat.myapplication.ui.detail.RepositoryDetailScreen
import com.fanirahmat.myapplication.ui.main.adapter.RepositoryAdapter
import android.text.Editable

import android.text.TextWatcher
import android.widget.AdapterView
import com.fanirahmat.myapplication.data.model.RepositoryResponse
import com.fanirahmat.myapplication.ui.main.adapter.SpinnerSortAdapter


class MainScreen : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MainViewModel
    private lateinit var adapter: RepositoryAdapter
    private var listSort = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.rvRepos.layoutManager = LinearLayoutManager(this)
        binding.rvRepos.setHasFixedSize(true)

        viewModel = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(MainViewModel::class.java)
        viewModel.getRepositories().observe(this, { listRepo ->
            if (listRepo.isEmpty()) {
                showLoading(true)
            } else {
                showLoading(false)
            }

            adapter = RepositoryAdapter(listRepo) {
                val i = Intent(this, RepositoryDetailScreen::class.java)
                i.putExtra("repo", it.name)
                startActivity(i)
            }
            binding.rvRepos.adapter = adapter
        })

        binding.etSearch.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                viewModel.searchRepositories(s.toString())
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {


            }

            override fun afterTextChanged(s: Editable) {

            }
        })
        showSpinnerSort()

    }

    private fun showSpinnerSort() {
        listSort.clear()
        listSort.add("stars")
        listSort.add("forks")
        listSort.add("updated")
        val adapter = SpinnerSortAdapter(this, listSort)
        binding.spinnerSort.adapter = adapter

        binding.spinnerSort.onItemSelectedListener = object :  AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                when(p0?.selectedItem.toString()) {
                    "stars"-> viewModel.sortRepositories("stars")
                    "forks"-> viewModel.sortRepositories("forks")
                    "updated"-> viewModel.setRepositories("updated")
                }
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

        }
    }

    private fun showLoading(state:Boolean) {
        if (state) {
            binding.progressBar.visibility = View.VISIBLE
        } else {
            binding.progressBar.visibility = View.GONE
        }
    }
}