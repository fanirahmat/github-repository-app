package com.fanirahmat.myapplication.ui.detail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.fanirahmat.myapplication.R
import com.fanirahmat.myapplication.databinding.ActivityRepositoryDetailScreenBinding
import com.fanirahmat.myapplication.ui.main.MainViewModel
import android.content.Intent
import android.net.Uri


class RepositoryDetailScreen : AppCompatActivity() {
    private lateinit var binding: ActivityRepositoryDetailScreenBinding
    private lateinit var viewModel: DetailViewModel
    private var repo:String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRepositoryDetailScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)

        repo = intent.getStringExtra("repo")

        viewModel = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(DetailViewModel::class.java)
        viewModel.setRepository(repo!!)
        viewModel.getRepository().observe(this, { dataRepo ->
            binding.apply {
                Glide.with(this@RepositoryDetailScreen)
                    .load(dataRepo.owner?.avatar_url)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(binding.ivAuthorDetail)

                tvRepoNameDetail.text = dataRepo.name
                tvAuthorDetail.text = dataRepo.owner?.login
                tvFullnameRepoDetail.text = dataRepo.full_name
                tvDefaultBranchDetail.text = dataRepo.default_branch
                tvLanguageDetail.text = dataRepo.language
                tvCreatedAt.text = dataRepo.created_at
                tvUpdateAt.text = dataRepo.updated_at

                btnOpenAuthor.setOnClickListener {
                    val browserIntent =
                        Intent(Intent.ACTION_VIEW, Uri.parse(dataRepo.owner?.html_url))
                    startActivity(browserIntent)
                }

                btnOpenRepo.setOnClickListener {
                    val browserIntent =
                        Intent(Intent.ACTION_VIEW, Uri.parse(dataRepo.html_url))
                    startActivity(browserIntent)
                }
            }
        })
        viewModel.getOwner().observe(this, { owner ->
            binding.apply {
                tvOwnerName.text = owner.login
                tvOwnerBio.text = owner.bio
                tvFollowers.text = owner.followers.toString()
            }
        })
    }
}