package com.fanirahmat.myapplication.ui.main.adapter

import android.view.LayoutInflater
import android.view.OnReceiveContentListener
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.fanirahmat.myapplication.data.model.RepositoryResponse
import com.fanirahmat.myapplication.databinding.ActivityMainBinding
import com.fanirahmat.myapplication.databinding.ItemRepositoryBinding

class RepositoryAdapter(
    private var list: List<RepositoryResponse>,
    private val listener: (RepositoryResponse) -> Unit
) : RecyclerView.Adapter<RepositoryAdapter.ViewHolder>(

) {

    inner class ViewHolder(
        private val binding: ItemRepositoryBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(repo: RepositoryResponse) {

            binding.apply {

                Glide.with(itemView)
                    .load(repo.owner?.avatar_url)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(binding.ivUser)

                tvReponame.text = repo.name
                tvAuthor.text = repo.owner?.login
                tvWatcher.text = "Watcher : "+repo.watchers_count.toString()
                tvForks.text = "Forks : "+repo.forks_count.toString()
                tvIssues.text = "Issues : "+repo.open_issues_count.toString()

                itemView.setOnClickListener {
                    listener(repo)
                }
            }

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = ItemRepositoryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }

}