package com.fanirahmat.myapplication.ui.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fanirahmat.myapplication.api.RetrofitClient
import com.fanirahmat.myapplication.data.model.RepositoryResponse
import com.fanirahmat.myapplication.functions
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

class MainViewModel : ViewModel() {
    val listRepo = ArrayList<RepositoryResponse>()
    private val listFilteredRepo = ArrayList<RepositoryResponse>()
    val liveDatalistRepo = MutableLiveData<List<RepositoryResponse>>()

    init {
        setRepositories("")
    }

    fun setRepositories(sort:String) {
        functions.getMainApiService()?.getRepositories(sort)?.enqueue(object : Callback<List<RepositoryResponse>> {
            override fun onResponse(
                call: Call<List<RepositoryResponse>>,
                response: Response<List<RepositoryResponse>>
            ) {
                if (response.isSuccessful) {
                    val body = response.body()
                    listRepo.clear()
                    listFilteredRepo.clear()
                    if (body != null) {
                        listRepo.addAll(body)
                    }
                    liveDatalistRepo.postValue(listRepo)

                }
            }

            override fun onFailure(call: Call<List<RepositoryResponse>>, t: Throwable) {
                Log.e("getRepositories", t.toString())
            }

        })
    }

    fun sortRepositories(param:String) {
        if (param == "forks") {
            Collections.sort(listRepo,
                Comparator<RepositoryResponse> { o1, o2 ->
                    o1.forks_count!!.compareTo(o2.forks_count!!)
                })
        } else {
            Collections.sort(listRepo,
                Comparator<RepositoryResponse> { o1, o2 ->
                    o1.stargazers_count!!.compareTo(o2.stargazers_count!!)
                })
        }

        liveDatalistRepo.postValue(listRepo)
    }

    fun searchRepositories(s: String) {
        listFilteredRepo.clear()
        for (a in listRepo) {
            val fn = a.name
            if (fn?.lowercase(Locale.ROOT)?.contains(s.lowercase(Locale.ROOT))!!) {
                listFilteredRepo.add(a)
            }
        }

        if (listFilteredRepo.isNotEmpty()) {
            liveDatalistRepo.postValue(listFilteredRepo)
        }
    }

    fun getRepositories() : LiveData<List<RepositoryResponse>> {
        return liveDatalistRepo
    }
}