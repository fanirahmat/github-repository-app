package com.fanirahmat.myapplication.api

import com.fanirahmat.myapplication.data.model.Owner
import com.fanirahmat.myapplication.data.model.RepositoryResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

interface Api {

    @GET("users/Fanirahmat/repos")
    @Headers("Authorization: token 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")
    fun getRepositories(
        @Query("sort") sort:String?
    ) : Call<List<RepositoryResponse>>

    @GET("repos/Fanirahmat/{repo}")
    @Headers("Authorization: token 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")
    fun getDetailRepository(
        @Path("repo") repo:String
    ) : Call<RepositoryResponse>

    @GET("users/Fanirahmat")
    @Headers("Authorization: token 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")
    fun getDetailOwnerRepository(
    ) : Call<Owner>

}