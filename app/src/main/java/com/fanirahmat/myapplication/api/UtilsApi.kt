package com.fanirahmat.myapplication.api

class UtilsApi {
    private var client: RetrofitClient = RetrofitClient()
    private val baseUrl = "https://api.github.com/"

    fun getBaseApiService(): Api? {
        return client.getClient(baseUrl)?.create(Api::class.java)
    }

}

val utilsApi = UtilsApi()