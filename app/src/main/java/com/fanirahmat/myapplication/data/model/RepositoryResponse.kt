package com.fanirahmat.myapplication.data.model

import com.google.gson.annotations.SerializedName

data class RepositoryResponse (
    @SerializedName("id") var id:Int? = null,
    @SerializedName("name") var name:String? = null,
    @SerializedName("owner") var owner:Owner? = null,
    @SerializedName("forks_count") var forks_count:Int? = null,
    @SerializedName("stargazers_count") var stargazers_count:Int? = null,
    @SerializedName("open_issues_count") var open_issues_count:Int? = null,
    @SerializedName("watchers_count") var watchers_count:Int? = null,
    @SerializedName("default_branch") var default_branch:String? = null,
    @SerializedName("full_name") var full_name:String? = null,
    @SerializedName("language") var language:String? = null,
    @SerializedName("html_url") var html_url:String? = null,
    @SerializedName("created_at") var created_at:String? = null,
    @SerializedName("updated_at") var updated_at:String? = null,
)