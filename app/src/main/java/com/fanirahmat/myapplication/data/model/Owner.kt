package com.fanirahmat.myapplication.data.model

import com.google.gson.annotations.SerializedName

data class Owner(
    @SerializedName("id") var id:Int? = null,
    @SerializedName("login") var login:String? = null,
    @SerializedName("avatar_url") var avatar_url:String? = null,
    @SerializedName("html_url") var html_url:String? = null,
    @SerializedName("url") var url:String? = null,

    @SerializedName("name") var name:String? = null,
    @SerializedName("bio") var bio:String? = null,
    @SerializedName("followers") var followers:Int? = null,

)
