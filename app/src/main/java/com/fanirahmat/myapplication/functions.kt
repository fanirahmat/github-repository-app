package com.fanirahmat.myapplication

import com.fanirahmat.myapplication.api.Api
import com.fanirahmat.myapplication.api.utilsApi

class Functions {
    fun getMainApiService(): Api? {
        return utilsApi.getBaseApiService()
    }
}

val functions = Functions()